#include "ctablero.h"
#include <windows.h>
#include <time.h>
#include <iostream>
using namespace std;

CTABLERO::CTABLERO()
{
    //ctor
}

CTABLERO::~CTABLERO()
{
    //dtor
}
void CTABLERO::tableroFacil(){
    Setnivel(5);
    Setvidas(10);
    fin=false;
    Setintentos(0);

    matriz[nivel][nivel];
    matrizVisible[nivel][nivel];
    srand(time(NULL));

    for(int i=0;i<nivel;i++){
        for(int j=0;j<nivel;j++){
            matriz[i][j]=1+rand()%2;
        }
    }
    for(int i=0;i<nivel;i++){
        for(int j=0;j<nivel;j++){
            matrizVisible[i][j]="#";
        }
    }
    ui.mostrarMensaje("\n\t\t - B U S C A M I N A S -\n");
    ui.mostrarMensaje("   Intentos: ");
    ui.mostrarDato(intentos);
    ui.mostrarMensaje("                         Vidas: ");
    ui.mostrarDato(vidas);
    ui.mostrarMensaje("\n");

    for(int i=0;i<nivel;i++){
        ui.mostrarMensaje("\n");
        for(int j=0;j<nivel;j++){
            cout<<"   "<<matrizVisible[i][j];
        }
        ui.mostrarMensaje("\n");
    }

    while(fin==false){
    pedirCasilla();
    }

}

void CTABLERO::tableroMedio(){
    Setnivel(8);
    Setvidas(6);
    fin=false;
    Setintentos(0);

    matriz[nivel][nivel];
    matrizVisible[nivel][nivel];
    srand(time(NULL));

    for(int i=0;i<nivel;i++){
        for(int j=0;j<nivel;j++){
            matriz[i][j]=1+rand()%2;
        }
    }

    for(int i=0;i<nivel;i++){
        for(int j=0;j<nivel;j++){
            matrizVisible[i][j]="#";
        }
    }

    ui.mostrarMensaje("\n\t\t - B U S C A M I N A S -\n");
    ui.mostrarMensaje("   Intentos: ");
    ui.mostrarDato(intentos);
    ui.mostrarMensaje("                         Vidas: ");
    ui.mostrarDato(vidas);
    ui.mostrarMensaje("\n");

    for(int i=0;i<nivel;i++){
        ui.mostrarMensaje("\n");
        for(int j=0;j<nivel;j++){
            cout<<"   "<<matrizVisible[i][j];
        }
        ui.mostrarMensaje("\n");
    }

    pedirCasilla();

}

void CTABLERO::tableroDificil(){
    Setnivel(10);
    Setvidas(3);
    fin=false;
    Setintentos(0);

    matriz[nivel][nivel];
    matrizVisible[nivel][nivel];
    srand(time(NULL));

    for(int i=0;i<nivel;i++){
        for(int j=0;j<nivel;j++){
            matriz[i][j]=1+rand()%2;
        }
    }

    for(int i=0;i<nivel;i++){
        for(int j=0;j<nivel;j++){
            matrizVisible[i][j]="#";
        }
    }

    ui.mostrarMensaje("\n\t\t - B U S C A M I N A S -\n");
    ui.mostrarMensaje("   Intentos: ");
    ui.mostrarDato(intentos);
    ui.mostrarMensaje("                         Vidas: ");
    ui.mostrarDato(vidas);
    ui.mostrarMensaje("\n\n");

    for(int i=0;i<nivel;i++){
        ui.mostrarMensaje("\n");
        for(int j=0;j<nivel;j++){
            cout<<"   "<<matrizVisible[i][j];
        }
        ui.mostrarMensaje("\n");
    }

    while(fin==false){
    pedirCasilla();
    }
}

void CTABLERO::pedirCasilla(){
    do{
    ui.mostrarMensaje("\n   Digite la fila entre 1 y ");
    ui.mostrarDato(nivel);
    ui.mostrarMensaje(": ");
    Setfila(ui.recibirDato());
    if(fila<1 || fila>nivel){
        ui.mostrarMensaje("\nERROR: INGRESE UN VALOR ENTRE 1 Y 5");
    }
    }while(fila<1 || fila>nivel);
    do{
    ui.mostrarMensaje("\n   Digite la columna entre 1 y ");
    ui.mostrarDato(nivel);
    ui.mostrarMensaje(": ");
    Setcolumna(ui.recibirDato());
    if(columna<1 || columna>nivel){
        ui.mostrarMensaje("\nERROR: INGRESE UN VALOR ENTRE 1 Y 5");
    }
    }while(columna<1 || columna>nivel);
    intentos++;

    if(matriz[fila-1][columna-1]%2==0){
        matrizVisible[fila-1][columna-1]=" ";
    }
    if(matriz[fila-1][columna-1]%2!=0){
        matrizVisible[fila-1][columna-1]="*";
        vidas--;
        if(vidas==0){
            fin=true;
        }
    }
    system("cls");
    if(fin==false){
        ui.mostrarMensaje("\n\t\t - B U S C A M I N A S -\n");
        ui.mostrarMensaje("   Intentos: ");
        ui.mostrarDato(intentos);
        ui.mostrarMensaje("                         Vidas: ");
        ui.mostrarDato(vidas);
        ui.mostrarMensaje("\n");
        for(int i=0;i<nivel;i++){
            ui.mostrarMensaje("\n");
            for(int j=0;j<nivel;j++){
                cout<<"   "<<matrizVisible[i][j];
            }
            ui.mostrarMensaje("\n");
        }
        ui.mostrarMensaje("\n");
    }


}
