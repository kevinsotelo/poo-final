#ifndef CUI_H
#define CUI_H


class CUI
{
    public:
        CUI();
        virtual ~CUI();
        int recibirDato();
        void mostrarMensaje(char*);
        void mostrarDato(int);

};

#endif // CUI_H
